package com.callsign.spring.jpa.postgresql;

import java.util.List;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.callsign.spring.jpa.postgresql.model.Delivery;
import com.callsign.spring.jpa.postgresql.model.Ticket;
import com.callsign.spring.jpa.postgresql.repository.DeliveryRepository;
import com.callsign.spring.jpa.postgresql.repository.TicketRepository;

import org.springframework.beans.factory.annotation.Autowired;

@Component
public class SchedulerService {

	@Autowired
	TicketRepository TicketRepository;
	@Autowired
	DeliveryRepository DeliveryRepository;
	private static Date lastExecution = new Date(0);
	
	@Scheduled(initialDelay = 5000, fixedRate = 10000) 
	public void createTickets() {
		Date now =  new Date();
		System.out.println("createTicket started");
		List<Delivery> deliveries = DeliveryRepository.search(now, lastExecution);
		System.out.println("deliveries deliveries.elngth="+deliveries.size());
		for(Delivery delivery:deliveries){
			Ticket ticket = new Ticket();
			if(delivery.getExpectedDeliveryTime().getTime()<now.getTime() && delivery.getExpectedDeliveryTime().getTime()>=lastExecution.getTime() ){
				if(delivery.getCustomerType().equals("VIP")){
					ticket.setPriority(1);
				}else{
					ticket.setPriority(2);
				}
			}else if(delivery.getRestaurantMeanTime().getTime()<now.getTime() && delivery.getRestaurantMeanTime().getTime()>=lastExecution.getTime() ){
				if(delivery.getCustomerType().equals("VIP")){
					ticket.setPriority(3);
				}else{
					ticket.setPriority(4);
				}
			}else if(delivery.getTimeToReachDestination().getTime()<now.getTime() && delivery.getTimeToReachDestination().getTime()>=lastExecution.getTime() ){
				if(delivery.getCustomerType().equals("VIP")){
					ticket.setPriority(5);
				}else{
					ticket.setPriority(6);
				}
			}

			ticket.setPriority(1);
			ticket.setDescription("Delivery Delayed");
			ticket.setTicketStatus("OPENED");
			ticket.setCreationTime(new Date());
			System.out.println("ticket will be saved");
			ticket.setDelivery(delivery);
			TicketRepository.save(ticket);
			delivery.setRead(true);
			DeliveryRepository.save(delivery);
		}
		

	}
}
