package com.callsign.spring.jpa.postgresql.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.callsign.spring.jpa.postgresql.model.Ticket;
import com.callsign.spring.jpa.postgresql.repository.TicketRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class TicketController {

	@Autowired
	TicketRepository TicketRepository;

	@PostMapping("/tickets")
	public ResponseEntity<List<Ticket>> getAllTickets() {
		try {
			List<Ticket> tickets = new ArrayList<Ticket>();
			TicketRepository.listTickets().forEach(tickets::add);
			
			if (tickets.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(tickets, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
