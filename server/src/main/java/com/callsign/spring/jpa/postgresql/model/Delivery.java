package com.callsign.spring.jpa.postgresql.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "delivery")
public class Delivery {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "delivery_id")
	private long deliveryId;

	@Column(name = "customer_type")
	private String customerType;

	@Column(name = "delivery_status")
	private String deliveryStatus;

	@Column(name = "expected_delivery_time")
	private Date expectedDeliveryTime;

	@Column(name = "current_distance_from_dest")
	private double currentDistanceFromDest;

	@Column(name = "time_to_reach_destination")
	private Date timeToReachDestination;

	@Column(name = "rider_rating")
	private int riderRating;

	@Column(name = "restaurant_meant_time")
	private Date restaurantMeanTime;

	@Column(name = "is_read")
	private boolean isRead = false;

	public Delivery() {

	}

	public Delivery(long deliveryId, String customerType, String deliveryStatus, Date expectedDeliveryTime,
			double currentDistanceFromDest, Date timeToReachDestination, int riderRating, Date restaurantMeanTime) {
		this.deliveryId = deliveryId;
		this.customerType = customerType;
		this.deliveryStatus = deliveryStatus;
		this.expectedDeliveryTime = expectedDeliveryTime;
		this.currentDistanceFromDest = currentDistanceFromDest;
		this.timeToReachDestination = timeToReachDestination;
		this.riderRating = riderRating;
		this.restaurantMeanTime = restaurantMeanTime;
	}

	public long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Date getExpectedDeliveryTime() {
		return expectedDeliveryTime;
	}

	public void setExpectedDeliveryTime(Date expectedDeliveryTime) {
		this.expectedDeliveryTime = expectedDeliveryTime;
	}

	public Double getCurrentDistanceFromDest() {
		return currentDistanceFromDest;
	}

	public void setCurrentDistanceFromDest(double currentDistanceFromDest) {
		this.currentDistanceFromDest = currentDistanceFromDest;
	}

	public Date getTimeToReachDestination() {
		return timeToReachDestination;
	}

	public void setTimeToReachDestination(Date timeToReachDestination) {
		this.timeToReachDestination = timeToReachDestination;
	}

	public int getRiderRating() {
		return riderRating;
	}

	public void setRiderRating(int riderRating) {
		this.riderRating = riderRating;
	}

	public Date getRestaurantMeanTime() {
		return restaurantMeanTime;
	}

	public void setRestaurantMeanTime(Date restaurantMeanTime) {
		this.restaurantMeanTime = restaurantMeanTime;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}


	@Override
	public String toString() {
		return "Delivery [currentDistanceFromDest=" + currentDistanceFromDest + ", customerType=" + customerType
				+ ", deliveryId=" + deliveryId + ", deliveryStatus=" + deliveryStatus + ", expectedDeliveryTime="
				+ expectedDeliveryTime + ", restaurantMeanTime=" + restaurantMeanTime + ", riderRating=" + riderRating
				+ ", isRead=" + isRead + ", timeToReachDestination=" + timeToReachDestination + "]";
	}

}
