package com.callsign.spring.jpa.postgresql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.callsign.spring.jpa.postgresql.model.Ticket;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query("SELECT t FROM Ticket t WHERE t.delivery.deliveryStatus<>'Order Delivered'"
     + " ORDER BY t.priority ASC")
    public List<Ticket> listTickets();
  
}
