package com.callsign.spring.jpa.postgresql.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.callsign.spring.jpa.postgresql.model.Delivery;
import com.callsign.spring.jpa.postgresql.repository.DeliveryRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class DeliveryController {

	@Autowired
	DeliveryRepository deliveryRepository;

	@PostMapping("/deliveries")
	public ResponseEntity<List<Delivery>> getAllDeliveries() {
		try {
			List<Delivery> deliveries = new ArrayList<Delivery>();
			deliveryRepository.findAll().forEach(deliveries::add);
			
			if (deliveries.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(deliveries, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/delivery")
	public ResponseEntity<Delivery> createDelivery(@RequestBody Delivery delivery) {
		try {
			System.out.println("incoming delivery="+delivery);
			Delivery _delivery = deliveryRepository
					.save(new Delivery(delivery.getDeliveryId(), delivery.getCustomerType(), delivery.getDeliveryStatus(), delivery.getExpectedDeliveryTime(),
					delivery.getCurrentDistanceFromDest(), delivery.getTimeToReachDestination(), delivery.getRiderRating(), delivery.getRestaurantMeanTime()));
			return new ResponseEntity<>(_delivery, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/delivery/{id}")
	public ResponseEntity<Delivery> updateDelivery(@PathVariable("id") long id, @RequestBody Delivery delivery) {
		Optional<Delivery> deliveryData = deliveryRepository.findById(id);
		System.out.println("Incoming delivery="+delivery);
		if (deliveryData.isPresent()) {
			Delivery _delivery = deliveryData.get();
			_delivery.setDeliveryStatus(delivery.getDeliveryStatus());
			_delivery.setCurrentDistanceFromDest(delivery.getCurrentDistanceFromDest());
			_delivery.setRiderRating(delivery.getRiderRating());
			return new ResponseEntity<>(deliveryRepository.save(_delivery), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
