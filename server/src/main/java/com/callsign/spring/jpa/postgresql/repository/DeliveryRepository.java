package com.callsign.spring.jpa.postgresql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.callsign.spring.jpa.postgresql.model.Delivery;

import java.util.List;
import java.util.Date;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    @Query("SELECT d FROM Delivery d WHERE  d.isRead=false AND ((d.expectedDeliveryTime < ?1 AND d.expectedDeliveryTime >= ?2)"
     + " OR (d.restaurantMeanTime < ?1 AND d.restaurantMeanTime >= ?2)"
     + " OR (d.timeToReachDestination < ?1 AND d.timeToReachDestination >= ?2))"
     + " ORDER BY expectedDeliveryTime ASC")
    public List<Delivery> search(Date now, Date previousExecutionTime);    
  
}
