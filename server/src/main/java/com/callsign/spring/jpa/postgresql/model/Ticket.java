package com.callsign.spring.jpa.postgresql.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ticket")
public class Ticket {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@OneToOne //(fetch = FetchType.LAZY)
	@JoinColumn(name = "delivery_id", referencedColumnName = "delivery_id")
	private Delivery delivery;

	@Column(name = "priority")
	private int priority;

	@Column(name = "ticket_status")
	private String ticketStatus;

	@Column(name = "description")
	private String description;

	@Column(name = "creation_time")
	private Date creationTime;

	public Ticket() {

	}

	public Ticket(long id, int priority, String ticketStatus, String description, Date creationTime) {
		this.id = id;
		this.priority = priority;
		this.ticketStatus = ticketStatus;
		this.description = description;
		this.creationTime = creationTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public long getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

}
