import  React, {useState} from "react";
import { ListDeliveries } from "./components/ListDeliveries";
import SignIn from "./components/SignIn";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { AddDelivery } from "./components/AddDelivery";
import DateAdapter from '@mui/lab/AdapterMoment';
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { ListTickets } from "./components/ListTickets";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

const App = () => {
  const [ token, setToken ] =   useState('')

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  if (!token) {
    //return <Login setToken={setToken} />
    return <SignIn setToken={setToken}/>
  }
  return (
    <LocalizationProvider dateAdapter={DateAdapter}>
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="List Orders" id="listDeliveriesTab" aria-controls="tab-panel-0"/>
          <Tab label="Add Order" id="addDeliveryTab" aria-controls="tab-panel-1" />
          <Tab label="List Tickets" id="listTicket" aria-controls="tab-panel-2" />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <ListDeliveries/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <AddDelivery/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <ListTickets/>
      </TabPanel>
    </Box>
    </LocalizationProvider>
  );
};

export default App;
