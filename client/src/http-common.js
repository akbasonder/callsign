import axios from "axios";

//baseURL: "http://localhost:8080/api",
//baseURL: "http://postgre1:8080/api",
export default axios.create({
  baseURL: "http://localhost:8080/api",
  headers: {
    "Content-type": "application/json"
  }
});