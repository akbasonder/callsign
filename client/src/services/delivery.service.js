import http from "../http-common";


export const  getTickets = () =>{
    return http.post("/tickets");
}

export const  getDeliveries = () =>{
    return http.post("/deliveries");
}

export const addDelivery = (delivery) =>{
    return http.post("/delivery", delivery);
}

export const updateDelivery = (delivery) =>{
    console.log('updating delivery', delivery)
    return http.put(`/delivery/${delivery.deliveryId}`, delivery);
}