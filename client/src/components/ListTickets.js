import React, { useCallback, useEffect, useState } from "react";
import { getTickets } from "../services/delivery.service";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import Box from "@mui/material/Box";
import moment from "moment";

export const ListTickets = () => {
  const [tickets, setTickets] = useState([]);
  const columns = [
    { field: 'id', headerName: 'ID', },
    { field: 'deliveryId', headerName: 'ORDER ID', },
    { field: "priority", headerName: 'Priority' },
    { field: "ticketStatus", headerName: 'Status' },
    { field: "creationTime", headerName: 'Creation Time', width: 175 },
    { field: "distance", headerName: 'Distance' },
    { field: "riderRating", headerName: 'Rating'},
  ];
  
  /*const handleCellEditCommit = React.useCallback(
    async (params) => {
      try {
        // Make the HTTP request to save in the backend
        const response = await mutateRow({
          id: params.id,
          [params.field]: params.value,
        });

        setRows((prev) =>
          prev.map((row) => (row.id === params.id ? { ...row, ...response } : row)),
        );
      } catch (error) {
        
        // Restore the row in case of error
        setRows((prev) => [...prev]);
      }
    },
    [mutateRow],
  );*/
  const listTickets = useCallback(() => {
    getTickets()
      .then((res) => {
        console.log("res", res);
        const customedTickets = []
        res.data.forEach((ticket)=>{
          ticket.creationTime = moment(ticket.creationTime).format('yyyy-MM-DD HH:mm')
          ticket.deliveryId = ticket.delivery.deliveryId
          ticket.distance = ticket.delivery.currentDistanceFromDest
          ticket.riderRating = ticket.delivery.riderRating
          customedTickets.push(ticket)
        })
        setTickets(customedTickets);
      })
      .catch((err) => console.log("err", err));
  }, []);

  useEffect(() => {
    listTickets();
  }, [listTickets]);


  return (
    <div style={{ height: 400, width: '100%' }}>
      <Box>Delivered Ordered are not shown here</Box>
      <DataGrid
        rows={tickets}
        columns={columns} 
        components={{
          Toolbar: GridToolbar,
        }}
      />
    </div>
  );


};
