import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import DateTimePicker from "@mui/lab/DateTimePicker";
import Button from "@mui/material/Button";
import { addDelivery } from "../services/delivery.service";
//import { makeStyles } from '@material-ui/styles'


export const AddDelivery = () => {
  //const classes = useStyles()
  const rowStyle = { marginTop: 20, width: 300 };
  const [customerType, setCustomerType] = useState("VIP");
  const [deliveryStatus, setDeliveryStatus] = useState("Order Received");
  const [expectedDeliveryTime, setExpectedDeliveryTime] = useState(new Date());
  const [distance, setDistance] = useState(500);
  const [riderRating, setRiderRating] = useState(5);
  const [restaurantMeanTime, setRestaurantMeanTime] = useState(new Date());
  const [timeToReach, setTimeToReach] = useState(new Date());
  const handleChangeCustomerType = (event) => {
    setCustomerType(event.target.value);
  };
  const handleChangeDeliveryStatus = (event) => {
    setDeliveryStatus(event.target.value);
  };
  const handleChangeDistance = (event) => {
    setDistance(event.target.value);
  };
  const handleChangeRiderRating = (event) => {
    setRiderRating(event.target.value);
  };
  const handleSubmit = () => {
    
    const delivery = {
      deliveryId : 15,
      customerType,
      deliveryStatus,
      expectedDeliveryTime,
      currentDistanceFromDest: distance,
      riderRating,
      restaurantMeanTime,
      timeToReachDestination: timeToReach
    }
    console.log('delivery', delivery)
    addDelivery(delivery)
    alert('Order Created')
  };

  const getRiderRating = (min, max) => {
    const result = [];
    for (let i = min; i <= max; i++) {
      result.push(i);
    }
    return result;
  };

  return (
    <Box
      component="form"
      noValidate
      autoComplete="off"
      flexDirection={"column"}
      display={"flex"}
    >
      <FormControl fullWidth>
        <InputLabel id="label-customerType">Customer Type</InputLabel>
        <Select
          labelId="label-customerType"
          id="select-customerType"
          value={customerType}
          label="Customer Type"
          onChange={handleChangeCustomerType}
          style={{ width: 300 }}
        >
          <MenuItem value={"VIP"}>VIP</MenuItem>
          <MenuItem value={"Loyal"}>Loyal</MenuItem>
          <MenuItem value={"New"}>New</MenuItem>
        </Select>
      </FormControl>
      <FormControl fullWidth style={{ marginTop: 20 }}>
        <InputLabel id="label-deliveryStatus">Delivery Status</InputLabel>
        <Select
          labelId="label-deliveryStatus"
          id="select-deliveryStatus"
          value={deliveryStatus}
          label="Delivery Status"
          onChange={handleChangeDeliveryStatus}
          style={{ width: 300 }}
        >
          <MenuItem value={"Order Received"}>Order Received</MenuItem>
          <MenuItem value={"Order Preparing"}>Order Preparing</MenuItem>
          <MenuItem value={"Order Pickedup"}>Order Pickedup</MenuItem>
          <MenuItem value={"Order Delivered"}>Order Delivered</MenuItem>
        </Select>
      </FormControl>
      <Box style={rowStyle}>
        <DateTimePicker
          renderInput={(props) => <TextField {...props} />}
          label="Expected Delivery Time"
          value={expectedDeliveryTime}
          onChange={(newValue) => {
            setExpectedDeliveryTime(newValue);
          }}
        />
      </Box>
      <TextField
        id="distance"
        label="Distance"
        style={rowStyle}
        value={distance}
        onChange={handleChangeDistance}
      />
      <FormControl fullWidth style={{ marginTop: 20 }}>
        <InputLabel id="label-riderRating">Rider Rating</InputLabel>
        <Select
          labelId="label-riderRating"
          id="select-riderRating"
          value={riderRating}
          label="Rider Rating"
          onChange={handleChangeRiderRating}
          style={{ width: 300 }}
        >
          {getRiderRating(0, 10).map((rating) => (
            <MenuItem value={rating}>{rating}</MenuItem>
          ))}
        </Select>
      </FormControl>
      <Box style={rowStyle}>
        <DateTimePicker
          renderInput={(props) => <TextField {...props} />}
          label="Restaurant Mean Time"
          value={restaurantMeanTime}
          onChange={(newValue) => {
            setRestaurantMeanTime(newValue);
          }}
        />
      </Box>
      <Box style={rowStyle}>
        <DateTimePicker
          renderInput={(props) => <TextField {...props} />}
          label="Time To Reach"
          value={timeToReach}
          onChange={(newValue) => {
            setTimeToReach(newValue);
          }}
        />
      </Box>
      <Box style={rowStyle}>
        <Button variant="contained" onClick={handleSubmit}>Submit</Button>
      </Box>
    </Box>
  );
};
