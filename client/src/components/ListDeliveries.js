import React, { useCallback, useEffect, useState } from "react";
import { getDeliveries, updateDelivery } from "../services/delivery.service";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import Box from "@mui/material/Box";
import moment from "moment";

const formatDate = (timestamp = "") => {
  // `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
  return moment(timestamp).format('yyyy-MM-DD HH:mm')
};

export const ListDeliveries = () => {
  const [deliveries, setDeliveries] = useState([]);
  const columns = [
    { field: "deliveryId", headerName: "ID" },
    { field: "customerType", headerName: "Type", editable: true },
    { field: "deliveryStatus", headerName: "Status", editable: true },
    {
      field: "expectedDeliveryTime",
      headerName: "Expected Delivery",
      width: 175,
    },
    {
      field: "restaurantMeanTime",
      headerName: "Mean Time",
      width: 175,
    },
    {
      field: "currentDistanceFromDest",
      headerName: "Distance",
      editable: true,
    },
    { field: "riderRating", headerName: "Rating", editable: true },
    {
      field: "timeToReachDestination",
      headerName: "Reach Time",
      width: 175,
    },
  ];

  const [editRowsModel, setEditRowsModel] = React.useState({});

  const handleEditRowsModelChange = React.useCallback((model, ...rest) => {
    console.log("edited cell2", model);
    console.log("rest", rest);
    setEditRowsModel(model);
  }, []);

  const listDeliveries = useCallback(() => {
    console.log("listDeliveries pressed");
    getDeliveries()
      .then((res) => {
        console.log("res", res);
        const customedDeliveries = [];
        res.data.forEach((delivery) => {
          delivery.expectedDeliveryTime = formatDate(delivery.expectedDeliveryTime);
          delivery.timeToReachDestination = formatDate(
            delivery.timeToReachDestination
          );
          delivery.restaurantMeanTime = formatDate(delivery.restaurantMeanTime);
          delivery.id = delivery.deliveryId;
          customedDeliveries.push(delivery);
        });
        setDeliveries(customedDeliveries);
      })
      .catch((err) => console.log("err", err));
  }, []);

  useEffect(() => {
    listDeliveries();
  }, [listDeliveries]);

  const handleRowEditStop = (params, event) => {
    console.log("paramsss", params);
    event.defaultMuiPrevented = true;
    updateDelivery(params.row)
      .then((res) => console.log("update successfull", res))
      .catch((err) => console.log("update error", err));
  };

  return (
    <div style={{ height: 400, width: "100%" }}>
      <Box>You can edit record status, distance and rating </Box>
      <DataGrid
        rows={deliveries}
        columns={columns}
        components={{
          Toolbar: GridToolbar,
        }}
        editRowsModel={editRowsModel}
        editMode="row"
        onEditRowsModelChange={handleEditRowsModelChange}
        //onRowEditCommit={handleEditRowsModelChange}
        onRowEditStop={handleRowEditStop}
      />
    </div>
  );
};
